<?php

namespace Tests;

use App\Core\TokenGenerator;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
  use CreatesApplication;
  use DatabaseTransactions;

  public function ownerAuthorizationHeader(): array
  {
    return $this->authorizationHeaderForUser('Owner');
  }

  public function adminAuthorizationHeader(): array
  {
    return $this->authorizationHeaderForUser('Admin');
  }

  public function userAuthorizationHeader(): array
  {
    return $this->authorizationHeaderForUser('User');
  }

  public function studentAuthorizationHeader(): array
  {
    return $this->authorizationHeaderForUser('Student');
  }

  public function authorizationHeaderForUser($firstName): array
  {
    $user = User::where('firstName', $firstName)->first();

    $token = TokenGenerator::generateAuthTokenForUser($user)['token'];

    return ['Authorization' => 'Bearer '.$token];
  }
}
