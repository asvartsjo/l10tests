<?php

namespace Tests\Unit\Services\UserService;

use App\Models\User;
use App\Services\AuditSignupEventService;
use App\Services\ExamNetService;
use App\Services\UserService;
use App\Services\UserVerificationService;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class UpdateUserInfoTest extends TestCase
{
  private $userService;
  private $examNetServiceStub;
  private $userVerficationServiceMock;
  private $userInfo = [
    'id' => 99999,
    'firstName' => 'Larare',
    'lastName' => 'Ett',
    'countryCode' => 'SE',
    'email' => 'larare1@exam.net',
    'school' => [
      'id' => 99999,
      'countryCode' => 'SE',
      'name' => 'Test School',
    ],
  ];

  public function setUp(): void
  {
    parent::setUp();

    $this->examNetServiceStub = $this->createStub(ExamNetService::class);

    $this->app->instance(ExamNetService::class, $this->examNetServiceStub);
    $this->userVerficationServiceMock = $this->getMockBuilder(UserVerificationService::class)
      ->setConstructorArgs([$this->examNetServiceStub, new AuditSignupEventService()])
      ->onlyMethods(['sendMailAfterUserCreation'])->getMock()
    ;
    $this->app->instance(UserVerificationService::class, $this->userVerficationServiceMock);
    $this->userService = $this->app->make(UserService::class);
  }

  public function testSignupUnverified()
  {
    $locale = 'sv-SE';
    // Configure the stub.
    $this->examNetServiceStub
      ->method('getUserActivity')
      ->willReturn([
        'currentStudentCount' => 0,
        'acceptedBy' => 0,
        'validDomain' => FALSE,
      ])
    ;

    $this->userVerficationServiceMock->expects($this->exactly(1))
      ->method('sendMailAfterUserCreation')
      ->with(
        $this->equalTo(FALSE), // verified
        $this->equalTo($this->userInfo['email']),
        $this->equalTo($locale),
      )
    ;

    $this->userService->updateUserInfo($this->userInfo, $locale);

    // assert that user is stored with email in user authorization
    $this->assertEquals($this->userInfo['email'], User::where('firstName', 'Larare')->first()->mail());

    $this->assertDatabaseHas('user_authorizations', [
      'identification' => 99999,
      'data' => 'larare1@exam.net',
    ]);
    $this->assertDatabaseHas('users', [
      'verified' => 'TO_BE_VERIFIED',
      'firstName' => 'Larare',
      'lastName' => 'Ett',
    ]);
  }

  public function testSignupAutoVerified()
  {
    $locale = 'sv-SE';
    // Configure the stub.
    $this->examNetServiceStub
      ->method('getUserActivity')
      ->willReturn([
        'currentStudentCount' => 21,
        'acceptedBy' => 1,
        'validDomain' => TRUE,
      ])
    ;

    $this->userVerficationServiceMock->expects($this->exactly(1))
      ->method('sendMailAfterUserCreation')
      ->with(
        $this->equalTo(TRUE), // verified
        $this->equalTo($this->userInfo['email']),
        $this->equalTo($locale),
      )
    ;

    $this->userService->updateUserInfo($this->userInfo, $locale);

    // assert that user is stored with email in user authorization
    $this->assertEquals($this->userInfo['email'], User::where('firstName', 'Larare')->first()->mail());
  }

  public function testLoginOrgChange()
  {
    $userInfo2 = [
      'id' => 99990,
      'firstName' => 'ABCD',
      'lastName' => 'EDGH',
      'countryCode' => 'SE',
      'email' => 'larare9@exam.net',
      'school' => [
        'id' => 99990,
        'countryCode' => 'SE',
        'name' => 'Test School',
      ],
    ];
    $locale = 'sv-SE';
    // Configure the stub.
    $this->examNetServiceStub
      ->method('getUserActivity')
      ->willReturn([
        'currentStudentCount' => 21,
        'acceptedBy' => 1,
        'validDomain' => TRUE,
      ])
    ;

    // Create user with old organization
    $this->userService->updateUserInfo($userInfo2, $locale);

    // copy userInfo and change organization
    $userInfo2['school']['id'] = 99980;

    // Update user with new organization
    $this->userService->updateUserInfo($userInfo2, $locale);

    // assert that user is associated with new organization
    $this->assertEquals($userInfo2['school']['id'], User::where('firstName', 'ABCD')->first()->organizations()->first()->examId);
  }
}
