<?php

namespace Tests\Unit\Services\CognitiveSearchService;

use App\Services\CognitiveSearchService;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class FilterSearchStringTest extends TestCase
{
  private $cognitiveSearchService;

  public function setUp(): void
  {
    parent::setUp();

    $this->cognitiveSearchService = new CognitiveSearchService();
  }

  public function testStripLeft()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\leftb'),
      'ab'
    );
  }

  public function testStripRight()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\rightb'),
      'ab'
    );
  }

  public function testStripBegin()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\beginb'),
      'ab'
    );
  }

  public function testStripEnd()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\endb'),
      'ab'
    );
  }

  public function testStripArray()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a{array}b'),
      'ab'
    );
  }

  public function testStripDfrac()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\dfracb'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\dfrac{b'),
      'ab'
    );
  }

  public function testStripFrac()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\fracb'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\frac{b'),
      'ab'
    );
  }

  public function testStripCdot()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\cdotb'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\cdot{b'),
      'ab'
    );
  }

  public function testStripInt()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\int_b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\int_b'),
      'ab'
    );
  }

  public function testStripSlashes()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\_b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\_b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a\\;\\_\\b'),
      'ab'
    );
  }

  public function testStripBrackets()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a{ }b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a{b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a}b'),
      'ab'
    );
  }

  public function testStripDollarSign()
  {
    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a{ }b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a{b'),
      'ab'
    );

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString('a}b'),
      'ab'
    );
  }

  public function testStripComplexOne()
  {
    $data = '<div>Ved å bruke regresjon har Lene i tillegg funnet modellen <em>h</em> gitt ved</div><div>&nbsp;<br style=\"mso-special-character:line-break;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1651127549093_1\" id=\"eqid_1651127549093_1\" data-latex=\"h\\left(x\\right)=1667x^3-100000x^2+18333x+10000\" data-advanced=\"false\">$$h\\left(x\\right)=1667x^3-100000x^2+18333x+10000$$</span>&nbsp;</div>';

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString($data),
      'Ved å bruke regresjon har Lene i tillegg funnet modellen h gitt ved h(x)=1667x^3-100000x^2+18333x+10000'
    );
  }

  public function testScriptComplexTwo()
  {
    $data = "<div>The masses of a group of students at a school were recorded in the following table.</div>\n<div style=\"text-align: center;\">\n<br>\n</div>\n\n<table style=\"width: 80%; text-align: center;\"><tbody><tr><td style=\"width: 50%; background-color: rgb(239, 239, 239);\">Mass (<em>m</em>) in kg\n</td>\n<td style=\"width: 50%; background-color: rgb(239, 239, 239);\">Frequency</td></tr>\n<tr><td style=\"width: 50.0000%;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_3\" id=\"eqid_1659812818828_3\" data-latex=\"40\\le m<45\" data-advanced=\"false\">$$40\\le m&lt;45$$</span>&nbsp;</td>\n<td style=\"width: 50.0000%;\">8</td></tr>\n<tr><td style=\"width: 50.0000%;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_4\" id=\"eqid_1659812818828_4\" data-latex=\"45\\le m<50\" data-advanced=\"false\">$$45\\le m&lt;50$$</span>&nbsp;</td>\n<td style=\"width: 50.0000%;\">9</td></tr>\n<tr><td style=\"width: 50.0000%;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_6\" id=\"eqid_1659812818828_6\" data-latex=\"50\\le m<55\" data-advanced=\"false\">$$50\\le m&lt;55$$</span>&nbsp;</td>\n<td style=\"width: 50.0000%;\">7</td></tr>\n<tr><td style=\"width: 50.0000%;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_7\" id=\"eqid_1659812818828_7\" data-latex=\"55\\le m<60\" data-advanced=\"false\">$$55\\le m&lt;60$$</span>&nbsp;</td>\n<td style=\"width: 50.0000%;\">9</td></tr>\n<tr><td style=\"width: 50.0000%;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_8\" id=\"eqid_1659812818828_8\" data-latex=\"60\\le m<65\" data-advanced=\"false\">$$60\\le m&lt;65$$</span>&nbsp;</td>\n<td style=\"width: 50.0000%;\">3</td></tr></tbody></table>";

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString($data),
      'The masses of a group of students at a school were recorded in the following table.    Mass (m ) in kg  Frequency  40le m<45  8  45le m<50  9  50le m<55  7  55le m<60  9  60le m<65  3'
    );
  }

  public function testScriptComplexThree()
  {
    $data = "Fill in the gaps in the table below with the midpoints of each class interval.<div><br></div>\n<div>\n\n</div>\n\n<table style=\"margin-right: calc(20%); width: 80%;\"><tbody><tr><td style=\"width: 39.0071%; background-color: rgb(239, 239, 239);\">\n<div style=\"text-align: center;\">Mass (<em>m</em>) in kg</div>\n</td>\n<td style=\"width: 33.3333%; background-color: rgb(239, 239, 239);\">\n<div style=\"text-align: center;\">Midpoint</div>\n</td>\n<td style=\"width: 131.457%; background-color: rgb(239, 239, 239);\">\n<div style=\"text-align: center;\">Frequency</div>\n</td></tr>\n<tr><td style=\"width: 39.0071%;\">\n<div style=\"text-align: center;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_14\" id=\"eqid_1659812818828_14\" data-latex=\"40\\le m<45\" data-advanced=\"false\">$$40\\le m&lt;45$$</span>&nbsp;</div>\n</td>\n<td style=\"width: 33.3333%;\">\n<div style=\"text-align: center;\"><span class=\"textEditorGap\" data-gap=\"true\" data-id=\"gapQuestion_5\" id=\"gapQuestion_5\">42.5</span></div>\n</td>\n<td style=\"width: 131.457%;\">\n<div style=\"text-align: center;\">8</div>\n</td></tr>\n<tr><td style=\"width: 39.0071%;\">\n<div style=\"text-align: center;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_15\" id=\"eqid_1659812818828_15\" data-latex=\"45\\le m<50\" data-advanced=\"false\">$$45\\le m&lt;50$$</span>&nbsp;</div>\n</td>\n<td style=\"width: 33.3333%;\">\n<div style=\"text-align: center;\"><span class=\"textEditorGap\" data-gap=\"true\" data-id=\"gapQuestion_6\" id=\"gapQuestion_6\">47.5</span></div>\n</td>\n<td style=\"width: 131.457%;\">\n<div style=\"text-align: center;\">9</div>\n</td></tr>\n<tr><td style=\"width: 39.0071%;\">\n<div style=\"text-align: center;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_16\" id=\"eqid_1659812818828_16\" data-latex=\"50\\le m<55\" data-advanced=\"false\">$$50\\le m&lt;55$$</span>&nbsp;</div>\n</td>\n<td style=\"width: 33.3333%;\">\n<div style=\"text-align: center;\"><span class=\"textEditorGap\" data-gap=\"true\" data-id=\"gapQuestion_7\" id=\"gapQuestion_7\">52.5</span></div>\n</td>\n<td style=\"width: 131.457%;\">\n<div style=\"text-align: center;\">7</div>\n</td></tr>\n<tr><td style=\"width: 39.0071%;\">\n<div style=\"text-align: center;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_17\" id=\"eqid_1659812818828_17\" data-latex=\"55\\le m<60\" data-advanced=\"false\">$$55\\le m&lt;60$$</span>&nbsp;</div>\n</td>\n<td style=\"width: 33.3333%;\">\n<div style=\"text-align: center;\"><span class=\"textEditorGap\" data-gap=\"true\" data-id=\"gapQuestion_8\" id=\"gapQuestion_8\">57.5</span></div>\n</td>\n<td style=\"width: 131.457%;\">\n<div style=\"text-align: center;\">9</div>\n</td></tr>\n<tr><td style=\"width: 39.0071%;\">\n<div style=\"text-align: center;\"><span contenteditable=\"false\" class=\"latex-equation eqid_1659812818828_18\" id=\"eqid_1659812818828_18\" data-latex=\"60\\le m<65\" data-advanced=\"false\">$$60\\le m&lt;65$$</span>&nbsp;</div>\n</td>\n<td style=\"width: 33.3333%;\">\n<div style=\"text-align: center;\"><span class=\"textEditorGap\" data-gap=\"true\" data-id=\"gapQuestion_9\" id=\"gapQuestion_9\">62.5</span></div>\n</td>\n<td style=\"width: 131.457%;\">\n<div style=\"text-align: center;\">3</div>\n</td></tr></tbody></table><div>\n</div>";

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString($data),
      'Fill in the gaps in the table below with the midpoints of each class interval.    Mass (m ) in kg   Midpoint   Frequency   40le m<45    42.5   8   45le m<50    47.5   9   50le m<55    52.5   7   55le m<60    57.5   9   60le m<65    62.5   3'
    );
  }

  public function testScriptComplexFour()
  {
    $data = '<div>Tone samler på filmer. Tabellen nedenfor viser hvor lange filmene hennes er.</div><div><br></div><table align="left" border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-table-overlap:never;mso-yfti-tbllook:1184;mso-table-lspace:7.05pt; margin-left:4.8pt;mso-table-rspace:7.05pt;margin-right:4.8pt;mso-table-anchor-vertical: paragraph;mso-table-anchor-horizontal:column;mso-table-left:center;mso-table-top: .05pt;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;"><tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:34.95pt;"><td style="width:111.4pt;border:solid windowtext 1.0pt;mso-border-alt: solid windowtext .5pt;background:#D9D9D9;mso-background-themecolor:background1; mso-background-themeshade:217;padding:0cm 5.4pt 0cm 5.4pt;height:34.95pt;" width="149"><div>Lengde i</div><div>minutter</div></td><td style="width:105.2pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; background:#D9D9D9;mso-background-themecolor:background1;mso-background-themeshade: 217;padding:0cm 5.4pt 0cm 5.4pt;height:34.95pt;" width="140"><div>Frekvens</div></td></tr><tr style="mso-yfti-irow:1;height:25.15pt;"><td style="width:111.4pt;border:solid windowtext 1.0pt;border-top: none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:25.15pt;" width="149"><span contenteditable="false" class="latex-equation eqid_1651142333638_1" id="eqid_1651142333638_1" data-latex="[60,80⟩" data-advanced="false">$$[60,80⟩$$</span>&nbsp;<div></div></td><td style="width:105.2pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.15pt;" width="140">40<div></div></td></tr><tr style="mso-yfti-irow:2;height:23.65pt;"><td style="width:111.4pt;border:solid windowtext 1.0pt;border-top: none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:23.65pt;" width="149"><span contenteditable="false" class="latex-equation eqid_1651142333638_2" id="eqid_1651142333638_2" data-latex="[80,120⟩" data-advanced="false">$$[80,120⟩$$</span>&nbsp;<div></div></td><td style="width:105.2pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:23.65pt;" width="140">100<div></div></td></tr><tr style="mso-yfti-irow:3;height:25.15pt;"><td style="width:111.4pt;border:solid windowtext 1.0pt;border-top: none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:25.15pt;" width="149"><span contenteditable="false" class="latex-equation eqid_1651142333638_3" id="eqid_1651142333638_3" data-latex="[120,140⟩" data-advanced="false">$$[120,140⟩$$</span>&nbsp;<div></div></td><td style="width:105.2pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.15pt;" width="140">40<div></div></td></tr><tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes;height:23.65pt;"><td style="width:111.4pt;border:solid windowtext 1.0pt;border-top: none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:23.65pt;" width="149"><span contenteditable="false" class="latex-equation eqid_1651142333638_4" id="eqid_1651142333638_4" data-latex="[140,180⟩" data-advanced="false">$$[140,180⟩$$</span>&nbsp;<div></div></td><td style="width:105.2pt;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:23.65pt;" width="140">20<div></div></td></tr></tbody></table>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString($data),
      'Tone samler på filmer. Tabellen nedenfor viser hvor lange filmene hennes er. Lengde i minutter Frekvens  [60,80⟩  40  [80,120⟩  100  [120,140⟩  40  [140,180⟩  20'
    );
  }

  public function testStripTable()
  {
    $data = '<div>Fyll ut tabellen nedenfor</div><div><br></div><div><br></div><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh: .5pt solid windowtext;mso-border-insidev:.5pt solid windowtext;\"><tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:19.0pt;\"><td style=\"width:40.8pt;border:solid windowtext 1.0pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div style=\"text-align: center;\"><em>t</em></div><div></div></td><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div>0</div></td><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div>0,5</div></td><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div>1</div></td><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div>1,5</div></td><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div>2</div></td><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div>2,5</div></td><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-left: none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:19.0pt;\" width=\"54\"><div>3</div></td></tr><tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes;height:20.4pt;\"><td style=\"width:40.8pt;border:solid windowtext 1.0pt;border-top: none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt; padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div style=\"text-align: center;\"><em>h(t)</em></div><div></div></td><td style=\"width:40.8pt;border-top:none;border-left:none;border-bottom: solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt: solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div></div></td><td style=\"width:40.8pt;border-top:none;border-left:none;border-bottom: solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt: solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div></div></td><td style=\"width:40.8pt;border-top:none;border-left:none;border-bottom: solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt: solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div></div></td><td style=\"width:40.8pt;border-top:none;border-left:none;border-bottom: solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt: solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div></div></td><td style=\"width:40.8pt;border-top:none;border-left:none;border-bottom: solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt: solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div></div></td><td style=\"width:40.8pt;border-top:none;border-left:none;border-bottom: solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt: solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div></div></td><td style=\"width:40.8pt;border-top:none;border-left:none;border-bottom: solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt: solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt: solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.4pt;\" width=\"54\"><div></div></td></tr></tbody></table>';

    $this->assertEquals(
      $this->cognitiveSearchService->filterSearchString($data),
      'Fyll ut tabellen nedenfor  t  0 0,5 1 1,5 2 2,5 3  h(t)'
    );
  }
}
