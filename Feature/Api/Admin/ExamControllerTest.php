<?php

namespace Tests\Feature\Api\Admin;

use App\Models\Exam;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ExamControllerTest extends TestCase
{
  public $examId;

  public function setUp(): void
  {
    parent::setUp();
    $this->examId = Exam::first('id')->id;
  }

  /**
   *  Verify that a user without permission cannot update or create settings.
   */
  public function testNoPermissionToUpdateSettings()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('PUT', "api/admin/exam/{$this->examId}/settings", ['publishedToCourse' => TRUE]);
    $response->assertStatus(403);

    $response = $this->withHeaders($headers)->getJson('api/admin/exams/published');
    $response->assertStatus(403);
  }

  // Verify that an admin user can create, update and delete settings.
  public function testAdminCanCreateUpdateAndDelete()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('PUT', "api/admin/exam/{$this->examId}/settings", ['publishedToCourse' => TRUE]);
    $response->assertStatus(204);
    $this->assertDatabaseHas('course_exam', [
      'examId' => $this->examId,
    ]);

    $response = $this->withHeaders($headers)->json('PUT', "api/admin/exam/{$this->examId}/settings", ['publishedToCourse' => TRUE, 'thumbnailUrl' => url('storage/exam/thumbnail/1.jpg')]);
    $response->assertStatus(204);
    $this->assertDatabaseHas('course_exam', [
      'examId' => $this->examId,
      'thumbnailUrl' => url('storage/exam/thumbnail/1.jpg'),
    ]);

    $response = $this->withHeaders($headers)->json('PUT', "api/admin/exam/{$this->examId}/settings", ['publishedToCourse' => FALSE]);
    $response->assertStatus(204);
    $this->assertDatabaseMissing('course_exam', [
      'examId' => $this->examId,
    ]);
  }

  public function testAdminCanListPublishedExams()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->getJson('api/admin/exams/published');
    $response->assertStatus(200);
    $response->assertJsonStructure([
      'exams' => [
        '*' => [
          'versionId',
          'id',
          'name',
          'material' => [
            'id',
            'name',
            'type',
          ],
          'course' => [
            'id',
            'name',
          ],
          'subject' => [
            'id',
            'name',
          ],
          'thumbnailUrl',
          'createdAt',
          'updatedAt',
          'exportedAt',
          'questionsCount',
        ],
      ],
    ]);
  }
}
