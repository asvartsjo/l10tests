<?php

namespace Tests\Feature\Api\Admin\User;

use App\Enums\UserVerificationStatus;
use App\Models\User;
use App\Models\UserAuthorization;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ProfileControllerTest extends TestCase
{
  public $userExistsId;
  public $userDontExistsId;

  public function setUp(): void
  {
    parent::setUp();
    $this->userExistsId = User::first('id')->id;
    $this->userDontExistsId = User::max('id') + 1;
  }

  /**
   *  Verify that a user without permission cannot update or create settings.
   */
  public function testThatNoAdminUserCantGetOrUpdateUser()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('PATCH', "api/admin/users/{$this->userExistsId}", ['firstName' => 'Test']);
    $response->assertStatus(403);

    $response = $this->withHeaders($headers)->getJson("api/admin/users/{$this->userExistsId}");
    $response->assertStatus(403);
  }

  // Verify that an admin user can create, update and delete settings.
  public function testAdminCanRetriveAndUpdateUser()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('PATCH', "api/admin/users/{$this->userExistsId}", ['firstName' => 'Test']);
    $response->assertStatus(200)->assertJsonStructure([
      'firstName',
      'lastName',
      'verified',
      'organizations' => [
        '*' => [
          'id',
          'name',
          'role',
        ],
      ],
    ]);

    $response = $this->withHeaders($headers)->getJson("api/admin/users/{$this->userExistsId}");
    $response->assertStatus(200)->assertJsonStructure([
      'firstName',
      'lastName',
      'verified',
      'organizations' => ['*' => [
        'id',
        'name',
        'role',
      ]],
    ]);

    $response = $this->withHeaders($headers)->getJson("api/admin/users/{$this->userDontExistsId}");
    $response->assertStatus(404);
  }

  // Verify that an admin user can preverify users.
  public function testAdminCanPreVerifyUser()
  {
    $headers = $this->adminAuthorizationHeader();

    $user = User::create([
      'firstName' => 'Larare',
      'lastName' => 'TVÅ',
      'countryId' => 'SE',
      'languageId' => NULL,
      'verified' => UserVerificationStatus::ToBeVerified,
    ]);
    UserAuthorization::create([
      'userId' => $user['id'],
      'identification' => 9999,
      'type' => 'EXAM',
      'data' => 'larare2@exam.net',
      'verifiedAt' => now(),
    ]);

    $response = $this->withHeaders($headers)->json('PUT', 'api/admin/users/preverify', ['emails' => ['larare2@exam.net', 'larare3@exam.net']]);

    // Verify existing emails
    $response->assertJsonFragment([
      'email' => 'larare2@exam.net',
      'verified' => UserVerificationStatus::Teacher,
    ]);
    $response->assertJsonMissing([
      'email' => 'larare3@exam.net',
    ]);

    // Add new emails to preverified table
    $this->assertDatabaseHas('pre_verified_emails', [
      'email' => 'larare3@exam.net',
    ]);
  }
}
