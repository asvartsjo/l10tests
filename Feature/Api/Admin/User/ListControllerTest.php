<?php

namespace Tests\Feature\Api\Admin\User;

use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ListControllerTest extends TestCase
{
  public $jsonResponseVerifiedUsers = [
    '*' => [
      'id',
      'type',
      'email',
      'firstName',
      'lastName',
      'countryId',
      'verified',
      'organizations' => [
        '*' => [
          'id',
          'parent',
          'examId',
          'name',
          'countryId',
          'languageId',
          'createdAt',
          'updatedAt',
        ],
      ],
    ],
  ];

  /**
   * A basic feature test example.
   */
  public function testNonAdminUsersCantListUsers()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->get('/api/admin/users');
    $response->assertStatus(403);

    $response = $this->withHeaders($headers)->get('/api/admin/users/unverified');
    $response->assertStatus(403);
  }

  public function testAdminUsersCanListUsers()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->get('/api/admin/users');
    $response->assertStatus(200)->assertJsonStructure($this->jsonResponseVerifiedUsers);

    $response = $this->withHeaders($headers)->get('/api/admin/users?search=Admin');
    $response->assertStatus(200)->assertJsonStructure($this->jsonResponseVerifiedUsers);

    $response = $this->withHeaders($headers)->get('/api/admin/users/unverified');
    $response->assertStatus(200)->assertJsonStructure([
      '*' => [
        'id',
        'firstName',
        'lastName',
        'countryId',
        'verified',
        'email',
        'createdAt',
      ],
    ]);
  }
}
