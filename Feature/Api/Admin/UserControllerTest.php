<?php

namespace Tests\Feature\Api\Admin;

use App\Core\TokenGenerator;
use App\Enums\Permissions;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class UserControllerTest extends TestCase
{
  public $headers;

  public function setUp(): void
  {
    parent::setUp();

    $userAdmin = self::getUserWithPermissions(['READ', 'WRITE', Permissions::ViewAdminUI, Permissions::MasqueradeAsAnyUser]); // Admin user because of Permissions::ViewAdminUI
    $token = TokenGenerator::generateAuthTokenForUser($userAdmin)['token'];
    $this->headers = ['Authorization' => 'Bearer '.$token];
  }

  public function testUserHasNoPermissionToMasqueradeAsAnotherUser()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('POST', 'api/admin/users/login/3');
    $response->assertStatus(403);
  }

  public function testPermissionToMasquerade()
  {
    // testAdminUserCannotUpdateAnotherUserIfTheyDiffInPermissions
    $someOtherUser = self::getUserWithPermissions(['DELETE', Permissions::ViewAdminUI, Permissions::MasqueradeAsAnyUser]);
    $response = $this->withHeaders($this->headers)->json('POST', "api/admin/users/login/{$someOtherUser->id}");
    $response->assertStatus(403);

    // testAdminUserCanUpdateAnotherUserIfSomeOtherUserHaveLessPermissions
    $someOtherUser = self::getUserWithPermissions(['READ', Permissions::ViewAdminUI, Permissions::MasqueradeAsAnyUser]);
    $response = $this->withHeaders($this->headers)->json('POST', "api/admin/users/login/{$someOtherUser->id}");
    $response->assertStatus(200);

    // testAdminUserCannotUpdateAnotherUserIfSomeOtherUserHaveMorePermissions
    $someOtherUser = self::getUserWithPermissions(['READ', 'WRITE', 'EDIT', Permissions::ViewAdminUI, Permissions::MasqueradeAsAnyUser]);
    $response = $this->withHeaders($this->headers)->json('POST', "api/admin/users/login/{$someOtherUser->id}");
    $response->assertStatus(403);

    // testAdminUserCanUpdateAnotherUserIfTheyDoNotDiffInPermissions
    $someOtherUser = self::getUserWithPermissions(['READ', 'WRITE', Permissions::ViewAdminUI, Permissions::MasqueradeAsAnyUser]);
    $response = $this->withHeaders($this->headers)->json('POST', "api/admin/users/login/{$someOtherUser->id}");
    $response->assertStatus(200);
  }

  public function getUserWithPermissions($permissions)
  {
    $user = User::factory()->create(['countryId' => 'SE']);

    $permissionsAddedToDatabase = collect($permissions)->map(fn ($permission) => Permission::updateOrCreate(['name' => $permission]));

    $role = Role::updateOrCreate(['name' => fake()->name()]);

    $role->syncPermissions($permissionsAddedToDatabase);

    $user->assignRole($role);

    return $user;
  }
}
