<?php

namespace Tests\Feature\Api\Admin;

use App\Models\Node;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class CourseControllerTest extends TestCase
{
  public $courseId;

  public function setUp(): void
  {
    parent::setUp();
    $this->courseId = Node::select('id')->whereType('COURSE')->first()->id;
  }

  /**
   *  Verify that a user without permission cannot update or create settings.
   */
  public function testNoPermissionToUpdateOrCreateSettings()
  {
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('PUT', "api/admin/course/{$this->courseId}/settings");
    $response->assertStatus(403);
  }

  /**
   * Verify that an admin user can create, update and delete settings.
   */
  public function testAdminCanCreateUpdateAndDelete()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('PUT', "api/admin/course/{$this->courseId}/settings", ['formula' => 'Test']);
    $response->assertStatus(204);
    $this->assertDatabaseHas('node_attributes', [
      'nodeId' => $this->courseId,
      'value' => 'Test',
    ]);

    $response = $this->withHeaders($headers)->json('PUT', "api/admin/course/{$this->courseId}/settings", ['formula' => 'Test2']);
    $response->assertStatus(204);
    $this->assertDatabaseHas('node_attributes', [
      'nodeId' => $this->courseId,
      'value' => 'Test2',
    ]);

    $response = $this->withHeaders($headers)->json('PUT', "api/admin/course/{$this->courseId}/settings", ['formula' => '']);
    $response->assertStatus(204);
    $this->assertDatabaseMissing('node_attributes', [
      'nodeId' => $this->courseId,
      'value' => 'Test',
    ]);
  }
}
