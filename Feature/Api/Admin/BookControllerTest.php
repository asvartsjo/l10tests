<?php

namespace Tests\Feature\Api\Admin;

use App\Enums\NodeType;
use App\Models\Node;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class BookControllerTest extends TestCase
{
  public function testNoPermissionToListPublicBooks()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('GET', 'api/admin/books/public');
    $response->assertStatus(403);
  }

  public function testGetListOfPublicBooks()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('GET', 'api/admin/books/public');
    $response->assertStatus(200);
    $obj = $this->json('GET', 'api/admin/books/public');
    $this->assertEquals(NodeType::Book->value, $obj->json()[0]['type']);
  }

  public function testAddingThumbnailBook()
  {
    $headers = $this->adminAuthorizationHeader();
    $bookId = Node::where('name', 'Book under test')->first()->id;
    $response = $this->withHeaders($headers)->json('PATCH', "api/admin/book/{$bookId}/settings", ['thumbnailUrl' => 'lalala']);
    $response->assertStatus(204);

    $this->assertDatabaseHas('node_attributes', [
      'nodeId' => $bookId,
      'value' => 'lalala',
    ]);
  }

  public function testPublishSomethingOtherThanBook()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('PATCH', 'api/admin/book/7/settings', ['publish' => TRUE]);
    $response->assertStatus(422);
  }

  public function testPublishBook()
  {
    $headers = $this->adminAuthorizationHeader();
    $bookId = Node::where('name', 'Book under test')->first()->id;

    $response = $this->withHeaders($headers)->json('PATCH', "api/admin/book/{$bookId}/settings", ['publish' => TRUE]);
    $response->assertStatus(204);

    $this->assertDatabaseHas('node_user', [
      'nodeId' => $bookId,
      'role' => 'PUBLIC',
    ]);
  }
}
