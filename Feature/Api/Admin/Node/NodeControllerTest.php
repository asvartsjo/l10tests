<?php

namespace Tests\Feature\Api\Admin\Node;

use App\Enums\NodeType;
use App\Models\Node;
use App\Models\User;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class NodeControllerTest extends TestCase
{
  public $nodeToBeDeleted;
  public $nodeWithChildren;

  public function setUp(): void
  {
    parent::setUp();

    $this->nodeToBeDeleted = User::whereFirstname('Admin')->first()->subjects()->create(['type' => NodeType::Subject, 'name' => 'Test node to be deleted']);

    $this->nodeWithChildren = Node::has('children')->where('type', '<>', NodeType::Curriculum)->first();
  }

  /**
   *  Verify that a user without permission cannot update or create settings.
   */
  public function testNoPermissionsForEndpoints()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('POST', 'api/admin/node', ['name' => 'Test node', 'type' => NodeType::Curriculum, 'parent' => 1]);
    $response->assertStatus(403);

    $response = $this->withHeaders($headers)->getJson("api/admin/node/{$this->nodeWithChildren->id}");
    $response->assertStatus(403);

    $response = $this->withHeaders($headers)->json('PUT', "api/admin/node/{$this->nodeWithChildren->id}", []);
    $response->assertStatus(403);

    $response = $this->withHeaders($headers)->json('DELETE', "api/admin/node/{$this->nodeWithChildren->id}", []);
    $response->assertStatus(403);
  }

  public function testAdminCreatingAndUpdatingNodes()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('POST', 'api/admin/node', ['name' => 'Test node', 'type' => NodeType::Book, 'parent' => 1]);

    $response->assertStatus(201)
      ->assertJsonStructure([
        'id',
        'type',
        'name',
        'parentId',
        'createdAt',
        'updatedAt',
      ])
    ;

    $response = $this->withHeaders($headers)->getJson("api/admin/node/{$this->nodeToBeDeleted->id}");
    $response->assertStatus(200)
      ->assertJsonStructure([
        'id',
        'type',
        'name',
        'parent',
        'children',
        'assets',
      ])
    ;

    $response = $this->withHeaders($headers)->json('POST', 'api/admin/node', ['name' => 'Test node', 'type' => NodeType::Curriculum, 'parent' => 'SE']);

    $response->assertStatus(201)
      ->assertJsonStructure([
        'id',
        'type',
        'name',
        'createdAt',
        'updatedAt',
      ])
    ;
    $response = $this->withHeaders($headers)->json('POST', 'api/admin/node', ['name' => 'Test node', 'type' => 'Läroplan', 'parent' => 'SE']);
    $response->assertStatus(422);
  }

  // public function testAdminDeletingNodes()
  // {
  //   $headers = $this->adminAuthorizationHeader();
  //   $response = $this->withHeaders($headers)->json('DELETE', "api/admin/node/{$this->nodeToBeDeleted->id}");
  //   $response->assertStatus(501);

  //   $response = $this->withHeaders($headers)->json('DELETE', "api/admin/node/{$this->nodeWithChildren->id}");
  //   $response->assertStatus(501);
  // }
}
