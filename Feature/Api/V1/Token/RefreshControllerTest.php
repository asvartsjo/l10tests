<?php

namespace Tests\Feature\Api\V1\Token;

use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class RefreshControllerTest extends TestCase
{
  /**
   * Missing authorization header - GET.
   */
  public function testRefreshTokenRequiresAuthorizationHeader()
  {
    $response = $this->json('GET', '/api/v1/token/refresh');
    $response->assertStatus(401);
  }

  /**
   * Authorized admin token refresh.
   */
  public function testSuccessfulAdminTokenRefresh()
  {
    $headers = $this->adminAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('GET', '/api/v1/token/refresh');
    $response->assertStatus(200);
    $response->assertJson(
      fn (AssertableJson $json) => $json->has('token')->has('validSeconds')
    );
  }

  /**
   * Authorized user token refresh.
   */
  public function testSuccessfulUserTokenRefresh()
  {
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('GET', '/api/v1/token/refresh');
    $response->assertStatus(200);
    $response->assertJson(
      fn (AssertableJson $json) => $json->has('token')->has('validSeconds')
    );
  }
}
