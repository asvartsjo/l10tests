<?php

namespace Tests\Feature\Api\V1\Signup\Email;

use App\Enums\UserVerificationStatus;
use App\Models\Country;
use App\Models\EmailVerificationCode;
use App\Models\Organization;
use App\Models\User;
use App\Services\CodeGeneratorService;
use App\Services\ExamNetService;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

const DUMMY_LONG_CODE_2 = '2f70697c-cd24-4e2e-a0c2-7e3fb8100b98';

class DummyCodeGeneratorService2 extends CodeGeneratorService
{
  public static function generateLongCode()
  {
    return DUMMY_LONG_CODE_2;
  }
}

/**
 * @internal
 *
 * @coversNothing
 */
class SignupControllerTest extends TestCase
{
  public $formData;
  public $userInfoFromExam;
  public $createdUserFromExam;
  private $examNetServiceStub;

  public function setUp(): void
  {
    parent::setUp();
    // Typical user info from Exam.net from ExamNetService method getUserInfo.

    $this->formData = [
      'firstName' => 'new',
      'lastName' => 'user',
      'email' => 'new@km.se',
      'code' => DUMMY_LONG_CODE_2,
      'password' => '12$dsfas!!f23',
      'subjects' => ['math'],
    ];

    $this->formData['countryCode'] = 'GB';
    $this->formData['examOrganizationId'] = 35;

    EmailVerificationCode::updateOrCreate(
      ['email' => $this->formData['email']],
      [
        'signupCode' => Hash::make(DUMMY_LONG_CODE_2),
      ]
    );

    $mock = new DummyCodeGeneratorService2();
    $this->app->instance(CodeGeneratorService::class, $mock);
  }

  public function testSignup()
  {
    $this->createdUserFromExam = [
      'id' => 99,
    ];
    $this->userInfoFromExam = [
      'id' => 99,
      'schoolId' => 999,
    ];
    $this->setExamNetServiceStub();

    // Without examOrganizationId should fail
    unset($this->formData['examOrganizationId']);
    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response->assertStatus(422);
    $this->formData['examOrganizationId'] = 35;

    // Without email should fail
    unset($this->formData['email']);
    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response->assertStatus(422);
    $this->formData['email'] = 'new@km.se';

    // More than 72 characters in password
    $this->formData['password'] = 'dajfjdkahlskhfladshfkajewhf2183293289!"#"!2321jkdjhalsfjdasljflkdakdaösjfklsdjfkjdaksaj0939204u93';
    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response->assertStatus(422);
    $this->formData['password'] = '12$dsfas!!f23';

    // Without countryCode should fail
    unset($this->formData['countryCode']);
    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response->assertStatus(422);
    $this->formData['countryCode'] = 'GB';

    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response
      ->assertStatus(200)
      ->assertJson([
        'successful' => TRUE,
        'verified' => FALSE,
      ])
    ;

    $this->assertDatabaseHas('user_authorizations', [
      'data' => $this->formData['email'],
      'identification' => $this->userInfoFromExam['id'],
    ]);
    $this->assertDatabaseHas('users', [
      'firstName' => $this->formData['firstName'],
      'lastName' => $this->formData['lastName'],
      'verified' => UserVerificationStatus::ToBeVerified,
    ]);
    $user = User::where('firstName', $this->formData['firstName'])->first();
    $organization = Organization::where('examId', $this->userInfoFromExam['schoolId'])->first();

    $this->assertDatabaseHas('organization_user', [
      'userId' => $user->id,
      'organizationId' => $organization->id,
      'role' => 'TEACHER',
    ]);
  }

  public function testSignupSwedishSchool()
  {
    $this->userInfoFromExam = [
      'id' => 100,
      'schoolId' => 1,
    ];

    $this->createdUserFromExam = [
      'id' => 100,
    ];

    $this->formData['examOrganizationId'] = 1;

    $this->setExamNetServiceStub();

    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);

    $response
      ->assertStatus(200)
      ->assertJson([
        'successful' => TRUE,
        'verified' => FALSE,
      ])
    ;
  }

  public function testSignupWithNewSchool()
  {
    EmailVerificationCode::updateOrCreate(
      ['email' => 'lala@km.se'],
      [
        'signupCode' => Hash::make(DUMMY_LONG_CODE_2),
      ]
    );

    $this->formData = [
      'firstName' => 'ronny',
      'lastName' => 'ale',
      'email' => 'lala@km.se',
      'code' => DUMMY_LONG_CODE_2,
      'password' => '12345678',
      'countryCode' => 'IN',
      'subjects' => [
        'math',
      ],
    ];

    $this->createdUserFromExam = [
      'id' => 99,
    ];

    $this->userInfoFromExam = [
      'id' => 99,
      'schoolId' => 168,
    ];
    $this->setExamNetServiceStub();

    // Test with no organization data. Should fail if no organization data is provided.
    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response->assertStatus(422);

    $this->formData['organization'] = [
      'type' => 'school',
      'name' => 'orgnameeee',
      // 'city' => 'Mumbbbaii',
      'studentCount' => '123',
      'regionId' => 3,
    ];
    // if something from organization is missing, like city, it should fail.
    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response->assertStatus(422);

    // This should create a new organization, schoolId 13 so it wont clash with existing school
    $this->userInfoFromExam = [
      'id' => 100,
      'schoolId' => 13,
    ];

    $this->formData['organization'] = [
      'type' => 'school',
      'name' => 'orgnameeee',
      'city' => 'Mumbbbaii',
      'studentCount' => '123',
      'regionId' => 3,
    ];
    $this->formData['countryCode'] = 'IN';

    $this->setExamNetServiceStub();

    $response = $this->json('POST', 'api/v1/signup/finalize', $this->formData);
    $response
      ->assertStatus(200)
      ->assertJson([
        'successful' => TRUE,
        'verified' => FALSE,
      ])
    ;

    // Assert that the database has the correct data
    $this->assertDatabaseHas('user_authorizations', [
      'data' => $this->formData['email'],
      'identification' => $this->userInfoFromExam['id'],
    ]);
    $this->assertDatabaseHas('users', [
      'firstName' => $this->formData['firstName'],
      'lastName' => $this->formData['lastName'],
      'verified' => UserVerificationStatus::ToBeVerified,
    ]);

    $schoolCountry = Country::find($this->formData['countryCode']);
    $this->assertDatabaseHas('organizations', [
      'examId' => $this->userInfoFromExam['schoolId'],
      'name' => $this->formData['organization']['name'],
      'countryId' => $this->formData['countryCode'],
      'languageId' => $schoolCountry->languages()->orderBy('order', 'ASC')->value('languageId'),
    ]);

    $user = User::where('firstName', $this->formData['firstName'])->first();
    $organization = Organization::where('examId', $this->userInfoFromExam['schoolId'])->first();

    $this->assertDatabaseHas('organization_user', [
      'userId' => $user->id,
      'organizationId' => $organization->id,
      'role' => 'TEACHER',
    ]);
  }

  public function setExamNetServiceStub()
  {
    $this->examNetServiceStub = $this->createStub(ExamNetService::class);
    $this->app->instance(ExamNetService::class, $this->examNetServiceStub);

    $this->examNetServiceStub
      ->method('createUser')
      ->willReturn([
        'id' => $this->createdUserFromExam['id'],
      ])
    ;

    $userInfo = [
      'id' => $this->userInfoFromExam['id'],
      'email' => $this->formData['email'],
      'firstName' => $this->formData['firstName'],
      'lastName' => $this->formData['lastName'],
      'country' => 'someCountry',
      'countryCode' => $this->formData['countryCode'],
      'school' => [
        'id' => $this->userInfoFromExam['schoolId'],
        'name' => isset($this->formData['organization']) ? $this->formData['organization']['name'] : '',
        'city' => 'someCityName',
        'country' => 13,
        'countryCode' => $this->formData['countryCode'],
        'accessTo' => '2023-03-01T13:06:16.000000Z',
        'hidden' => 0,
        'createdAt' => '2023-03-01T13:06:16.000000Z',
        'updatedAt' => '2023-03-01T13:06:16.000000Z',
      ],
    ];

    $this->examNetServiceStub
      ->method('getUserInfo')
      ->willReturn($userInfo)
    ;
  }
}
