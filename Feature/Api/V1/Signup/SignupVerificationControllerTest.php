<?php

namespace Tests\Feature\Api\V1\Signup\Email;

use App\Models\EmailVerificationCode;
use App\Services\CodeGeneratorService;
use App\Services\ExamNetClientService;
use App\Services\ExamNetService;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

const DUMMY_SHORT_CODE = 'abc1';
// const DUMMY_WRONG_SHORT_CODE = 'cba123';
const DUMMY_LONG_CODE = '2f70697c-cd24-4e2e-a0c2-7e3fb8100b98';

const USER_OBJECT = [
  'userId' => 2,
  'email' => 'jd@school.edu',
];

class DummyCodeGeneratorService extends CodeGeneratorService
{
  public static function generateShortCode($length = 4)
  {
    return DUMMY_SHORT_CODE;
  }

  public static function generateLongCode()
  {
    return DUMMY_LONG_CODE;
  }
}

class DummyExamNetService extends ExamNetService
{
  public function findUserByEmail($email): ?int
  {
    return USER_OBJECT['email'] === $email ? USER_OBJECT['userId'] : NULL;
  }

  public function isDomainAcceptable(string $domain)
  {
    return 'gmail.com' === $domain ? FALSE : TRUE;
  }
}

/**
 * @internal
 *
 * @coversNothing
 */
class SignupVerificationControllerTest extends TestCase
{
  use WithoutMiddleware;

  public function setUp(): void
  {
    parent::setUp();

    $mock = new DummyExamNetService(new ExamNetClientService());
    $this->app->instance(ExamNetService::class, $mock);
  }

  public function testCodePersisted()
  {
    $email = 'test@example.com';
    // Call endpoint
    $response = $this->json('POST', '/api/v1/signup/verification/send', ['email' => $email]);

    // Check DB
    $emailCode = EmailVerificationCode::where('email', $email)->first();

    // Assert
    $response->assertStatus(204);

    $this->assertStringContainsString('$2y', $emailCode->shortCode);
    $this->assertStringContainsString('$2y', $emailCode->longCode);
    $this->assertNull($emailCode->signupCode);
  }

  public function testCodeVerification()
  {
    $mock = new DummyCodeGeneratorService();
    $this->app->instance(CodeGeneratorService::class, $mock);

    $email = 'test2@example.com';
    // Call endpoint
    $response = $this->json('POST', '/api/v1/signup/verification/send', ['email' => $email]);

    // second invocation works
    $response = $this->json('POST', '/api/v1/signup/verification/send', ['email' => $email]);
    $response->assertStatus(204);

    $response = $this->json('POST', '/api/v1/signup/verification/verify', ['email' => $email, 'code' => DUMMY_SHORT_CODE]);
    $response->assertStatus(200);
    // short codes expire on use
    $response = $this->json('POST', '/api/v1/signup/verification/verify', ['email' => $email, 'code' => DUMMY_SHORT_CODE]);
    $response->assertStatus(422);

    // long codes do not expire
    $response = $this->json('POST', '/api/v1/signup/verification/verify', ['email' => $email, 'code' => DUMMY_LONG_CODE]);
    $response->assertStatus(200);
    $response = $this->json('POST', '/api/v1/signup/verification/verify', ['email' => $email, 'code' => DUMMY_LONG_CODE]);
    $response->assertStatus(200);
  }

  public function testCorrectEmailIsNeeded()
  {
    $mock = new DummyCodeGeneratorService();
    $this->app->instance(CodeGeneratorService::class, $mock);

    $email = 'test2@example.com';
    // Call endpoint
    $response = $this->json('POST', '/api/v1/signup/verification/send', ['email' => $email]);

    // disallow other emails to use this short code
    $response = $this->json('POST', '/api/v1/signup/verification/verify', ['email' => 'wrong@example.com', 'code' => DUMMY_SHORT_CODE]);
    $response->assertStatus(400);

    // disallow other emails to use this long code
    $response = $this->json('POST', '/api/v1/signup/verification/verify', ['email' => 'wrong@example.com', 'code' => DUMMY_LONG_CODE]);
    $response->assertStatus(400);

    // code still works
    $response = $this->json('POST', '/api/v1/signup/verification/verify', ['email' => $email, 'code' => DUMMY_SHORT_CODE]);
    $response->assertStatus(200);
  }

  /**
   * User email already exists.
   */
  public function testUserEmailAlreadyExists()
  {
    $response = $this->json('GET', '/api/v1/signup/check', ['email' => USER_OBJECT['email']]);

    $response->assertStatus(200)->assertJson([
      'status' => 'ok',
    ]);
  }

  /**
   * User email does not exists.
   */
  public function testUserEmailDoesNotExists()
  {
    $response = $this->json('GET', '/api/v1/signup/check', ['email' => 'en@emailadress.edu']);

    $response->assertStatus(200)->assertJson([
      'status' => 'ok',
    ]);
  }

  /**
   * Email not allowed.
   */
  public function testErrorEmail()
  {
    $response = $this->json('GET', '/api/v1/signup/check', ['email' => 'emailadress.edu']);

    $response->assertStatus(422);

    $response->assertJson(
      [
        'message' => 'The email must be a valid email address.',
        'errors' => [
          'email' => [
            'The email must be a valid email address.',
          ],
        ],
      ]
    );
  }

  /**
   * Missing email .
   */
  public function testErrorNoEmail()
  {
    $response = $this->json('GET', '/api/v1/signup/check', ['email' => '']);
    $response->assertStatus(422);
    $response->assertJson(
      [
        'message' => 'The email field is required.',
        'errors' => [
          'email' => [
            'The email field is required.',
          ],
        ],
      ]
    );
  }

  /**
   * Generic email .
   */
  public function testGenericEmail()
  {
    $response = $this->json('GET', '/api/v1/signup/check', ['email' => 'larare1@gmail.com']);

    $response->assertStatus(200)->assertJson([
      'status' => 'generic',
    ]);
  }
}
