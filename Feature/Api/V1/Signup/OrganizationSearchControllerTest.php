<?php

namespace Tests\Feature\Api\V1\Signup;

use App\Models\EmailVerificationCode;
use App\Services\ExamNetService;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class OrganizationSearchControllerTest extends TestCase
{
  use WithoutMiddleware;
  private $dummyCode = 'abc123';
  private $dummyEmail = 'andreas.svartsjo@exam.net';

  public function setUp(): void
  {
    parent::setUp();
    EmailVerificationCode::create(
      [
        'email' => $this->dummyEmail,
        'signupCode' => Hash::make($this->dummyCode),
      ]
    );
    $this->examNetServiceStub = $this->createStub(ExamNetService::class);
    $this->app->instance(ExamNetService::class, $this->examNetServiceStub);

    $this->examNetServiceStub
      ->method('getSchools')
      ->willReturn([
        [
          'id' => '1',
          'name' => 'School One',
          'city' => 'Stockholm',
        ],
      ])
    ;

    $this->examNetServiceStub
      ->method('suggestedSchools')
      ->willReturn([
        [
          'id' => '1',
          'name' => 'School One',
          'city' => 'Stockholm',
        ],
      ])
    ;
  }

  public function testSearchByCountry()
  {
    $response = $this->json('GET', '/api/v1/signup/org/by-country', ['country' => 'SE']);

    // Assert
    $response->assertStatus(200)->assertJsonStructure([
      'schools' => [
        '*' => [
          'id',
          'name',
          'city',
        ],
      ],
    ]);
  }

  public function testSearchByEmail()
  {
    $response = $this->json('POST', '/api/v1/signup/org/by-email', ['email' => $this->dummyEmail, 'code' => $this->dummyCode]);

    // Assert
    $response->assertStatus(200)->assertJsonStructure([
      'suggestedSchools' => [
        '*' => [
          'id',
          'name',
          'city',
        ],
      ],
    ]);
  }

  public function testSearchByIncorrectEmail()
  {
    $response = $this->json('POST', '/api/v1/signup/org/by-email', ['email' => 'incorrect@example.com', 'code' => $this->dummyCode]);

    // Assert
    $response->assertStatus(400);
  }

  public function testSearchByIncorrectCode()
  {
    $response = $this->json('POST', '/api/v1/signup/org/by-email', ['email' => $this->dummyEmail, 'code' => 'incorrect']);

    // Assert
    $response->assertStatus(400);
  }
}
