<?php

namespace Tests\Feature\Api\V1;

use App\Models\Country;
use App\Services\ExamNetService;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class CountryControllerTest extends TestCase
{
  private $country;
  private $countryWithNodes;
  private $examNetServiceStub;

  public function setUp(): void
  {
    parent::setUp();

    $this->examNetServiceStub = $this->createStub(ExamNetService::class);
    $this->app->instance(ExamNetService::class, $this->examNetServiceStub);
    $this->country = Country::whereEnabled(1)->first('id');

    $this->countryWithNodes = Country::withWhereHas('nodes')->first('id');
  }

  public function testCountriesEndpoints()
  {
    $response = $this->getJson('/api/v1/countries');
    $response->assertStatus(200)->assertJsonStructure([
      'countries' => [
        '*' => [
          'id',
          'name',
          'native',
          'languages' => [
            '*' => [
              'id',
              'order',
            ],
          ],
        ],
      ],
    ]);
  }

  public function testCountryRegionsEndpoint()
  {
    $this->examNetServiceStub
      ->method('getRegions')
      ->willReturn([
        [
          'id' => 2,
          'region' => 'Calabria',
        ],
      ])
    ;

    $response = $this->getJson("/api/v1/country/{$this->country->id}/regions");

    $response->assertStatus(200)->assertJsonStructure([
      'regions' => [
        '*' => [
          'id',
          'region',
        ],
      ],
    ]);
  }

  public function testCountryNodeEndpoint()
  {
    $response = $this->getJson("/api/v1/country/{$this->countryWithNodes->id}/node");
    $response->assertStatus(200)->assertJsonStructure([
      'nodes' => [
        '*' => [
          'id',
          'type',
          'name',
        ],
      ],
    ]);
  }
}
