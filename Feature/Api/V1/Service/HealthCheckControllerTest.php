<?php

namespace Tests\Feature\Api\V1\Service;

use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class HealthCheckControllerTest extends TestCase
{
  /**
   * A basic feature test example.
   */
  public function testHealthcheck()
  {
    $response = $this->getJson('/api/v1/service/healthcheck');

    $response->assertStatus(200)->assertJson(['succesful' => TRUE]);
  }
}
