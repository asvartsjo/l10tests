<?php

namespace Tests\Feature\Api\V1\Auth;

use App\Enums\UserVerificationStatus;
use App\Models\User;
use App\Services\ExamNetService;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class EmailLoginControllerTest extends TestCase
{
  // Typical user info from Exam.net from ExamNetService method getUserInfo.
  public $userInfo;
  public $tokenData;
  private $token = '';
  private $examNetServiceStub;

  public function setUp(): void
  {
    parent::setUp();
    // Typical user info from Exam.net from ExamNetService method getUserInfo.

    $this->userInfo = [
      'id' => 2,
      'email' => 'larare1@exam.net',
      'firstName' => 'Lärare',
      'lastName' => 'Ett',
      'country' => 'Sweden',
      'countryCode' => 'SE',
      'school' => [
        'id' => 1,
        'name' => 'En svensk skola',
        'city' => 'Göteborg',
        'country' => 1,
        'accessTo' => '2024-03-01T13:06:16.000000Z',
        'hidden' => 0,
        'createdAt' => '2023-03-01T13:06:16.000000Z',
        'updatedAt' => '2023-03-01T13:06:16.000000Z',
        'countryCode' => 'DE',
      ],
    ];

    $validSeconds = config('jwt.auth_token_valid_time');

    $this->tokenData = [
      'aud' => 'gauss',
      'iss' => 'exam.net',
      'exp' => now()->timestamp + $validSeconds,
    ];
  }

  public function testLoginWithVerifiedTeacher()
  {
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/exam', ['token' => $this->token]);

    $response->assertStatus(200);
    $response->assertJsonStructure(
      [
        'verified',
        'token',
        'validSeconds',
      ],
    );
  }

  public function testLoginWithBadToken()
  {
    $this->tokenData['aud'] = 'NOT_GAUSS';
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/exam', ['token' => $this->token]);
    $response->assertStatus(422);
    $this->tokenData['iss'] = 'NOT_EXAM.NET';
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/exam', ['token' => $this->token]);
    $response->assertStatus(422);

    // Check that the token is expired
    $this->tokenData['aud'] = 'gauss';
    $this->tokenData['exp'] = now()->timestamp - 50;
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/exam', ['token' => $this->token]);
    $response->assertStatus(401);
  }

  public function testLoginWithUnverifiedTeacher()
  {
    $user = User::find(4); // Unverified
    $this->userInfo['userId'] = $user->id;

    $this->userInfo['id'] = $user->id;

    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/exam', ['token' => $this->token]);

    $response->assertStatus(200)->assertJson(
      [
        'verified' => FALSE,
      ]
    );
  }

  public function testLoginAlreadyVerifiedButDoesNotExistInGauss()
  {
    $this->userInfo['id'] = 99999;
    $this->userInfo['firstName'] = 'New';
    $this->userInfo['lastName'] = 'inGauss';
    $this->userInfo['email'] = 'larare3@exam.net';
    $this->userInfo['school']['id'] = 999;

    $this->setTokenAndStubsWithCurrentUserInfo();

    $this->examNetServiceStub
      ->method('getUserActivity')
      ->willReturn([
        'currentStudentCount' => 21,
        'acceptedBy' => 1,
        'validDomain' => TRUE,
      ])
    ;

    $this->assertDatabaseMissing('user_authorizations', [
      'identification' => 99999,
    ]);
    $response = $this->json('POST', 'api/v1/auth/exam', ['token' => $this->token]);
    $decoded = JWT::decode(
      $response['token'],
      new Key(config('jwt.secret'), 'HS256')
    );

    $this->assertDatabaseHas('user_authorizations', [
      'identification' => 99999,
    ]);
    $this->assertDatabaseHas('users', [
      'id' => $decoded->sub,
      'firstName' => $this->userInfo['firstName'],
      'lastName' => $this->userInfo['lastName'],
      'verified' => UserVerificationStatus::Teacher,
    ]);
    $this->assertDatabaseHas('organization_user', [
      'userId' => $decoded->sub,
      'organizationId' => 4,
    ]);
  }

  public function testChangedUserInfoFromExamnet()
  {
    $this->userInfo['lastName'] = 'TVÅ';
    $this->userInfo['email'] = 'larare2@exam.net';
    $this->userInfo['school']['id'] = 999;
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/exam', ['token' => $this->token]);
    $response->assertStatus(200)->assertJsonStructure(
      [
        'verified',
        'token',
        'validSeconds',
      ],
    );

    $this->assertDatabaseHas('users', [
      'lastName' => 'TVÅ',
    ]);
    $this->assertDatabaseMissing('users', [
      'lastName' => 'Ett',
    ]);
    $this->assertDatabaseHas('user_authorizations', [
      'data' => 'larare2@exam.net',
    ]);
    $this->assertDatabaseMissing('user_authorizations', [
      'data' => 'larare1@exam.net',
    ]);
    $this->assertDatabaseHas('organization_user', [
      'organizationId' => 4,
      'userId' => 1,
    ]);
    $this->assertDatabaseMissing('organization_user', [
      'organizationId' => 1,
      'userId' => 1,
    ]);
  }

  public function setTokenAndStubsWithCurrentUserInfo()
  {
    $privateKey = env('EXAM_PRIVATE_KEY');
    $claims = [
      'userId' => $this->userInfo['id'],
      'organizationId' => $this->userInfo['school']['id'],
      'aud' => $this->tokenData['aud'],
      'iss' => $this->tokenData['iss'],
      'exp' => $this->tokenData['exp'], // Token from Exam contains this but is not used in ExamAuthenticate
      // 'firstName' => $this->userInfo['firstName'], Token from Exam contains this but is not used in ExamAuthenticate
      // 'lastName' => $this->userInfo['lastName'], Token from Exam contains this but is not used in ExamAuthenticate
    ];

    // Encode fake token from Exam.net with claims used in ExamAuthenticate
    $this->token = JWT::encode($claims, $privateKey, 'EdDSA');

    $this->examNetServiceStub = $this->createStub(ExamNetService::class);
    $this->app->instance(ExamNetService::class, $this->examNetServiceStub);

    $this->examNetServiceStub
      ->method('getUserInfo')
      ->willReturn($this->userInfo)
    ;
  }
}
