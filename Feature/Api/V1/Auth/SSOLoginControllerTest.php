<?php

namespace Tests\Feature\Api\V1\Auth;

use App\Models\EmailVerificationCode;
use App\Models\User;
use App\Models\UserAuthorization;
use App\Services\CodeGeneratorService;
use App\Services\ExamNetService;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
const DUMMY_LONG_CODE = '2f70697c-cd24-4e2e-a0c2-7e3fb8100b98';

class DummyCodeGeneratorService extends CodeGeneratorService
{
  public static function generateLongCode()
  {
    return DUMMY_LONG_CODE;
  }
}

/**
 * @internal
 *
 * @coversNothing
 */
class SSOLoginControllerTest extends TestCase
{
  public $userInfo;
  public $tokenData;
  public $stubData;

  private $token = '';
  private $examNetServiceStub;

  public function setUp(): void
  {
    parent::setUp();
    // Typical user info from Exam.net from ExamNetService method getUserInfo.

    $this->userInfo = [
      'id' => 2,
      'email' => 'larare1@exam.net',
      'firstName' => 'Lärare',
      'lastName' => 'Ett',
      'country' => 'Sweden',
      'countryCode' => 'SE',
      'school' => [
        'id' => 1,
        'name' => 'En svensk skola',
        'city' => 'Göteborg',
        'country' => 1,
        'accessTo' => '2024-03-01T13:06:16.000000Z',
        'hidden' => 0,
        'createdAt' => '2023-03-01T13:06:16.000000Z',
        'updatedAt' => '2023-03-01T13:06:16.000000Z',
        'countryCode' => 'DE',
      ],
    ];
    $validSeconds = config('jwt.auth_token_valid_time');

    $this->tokenData = [
      'aud' => 'gauss',
      'userType' => 'login',
      'exp' => now()->timestamp + $validSeconds,
    ];

    $this->stubData['findUserByEmail'] = (int) UserAuthorization::find(1)->identification;

    $mock = new DummyCodeGeneratorService();
    $this->app->instance(CodeGeneratorService::class, $mock);
  }

  public function testLoginWithVerifiedTeacher()
  {
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/sso', ['token' => $this->token]);

    $response->assertStatus(200);
    $response->assertJsonStructure(
      [
        'verified',
        'token',
        'validSeconds',
      ],
    );
  }

  public function testLoginWithUnverifiedTeacher()
  {
    $user = User::find(4); // Unverified
    $this->userInfo['id'] = $user->id;

    $this->tokenData['aud'] = 'gauss';
    $this->tokenData['userType'] = 'login';
    $this->stubData['findUserByEmail'] = $user->id;

    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/sso', ['token' => $this->token]);

    $response->assertStatus(200)->assertJson(
      [
        'type' => 'login',
        'verified' => FALSE,
      ]
    );
  }

  public function testLoginWithBadToken()
  {
    $this->tokenData['aud'] = 'NOT_GAUSS';
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/sso', ['token' => $this->token]);
    $response->assertStatus(422);

    $this->tokenData['aud'] = 'gauss';
    $this->stubData['findUserByEmail'] = NULL;
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/sso', ['token' => $this->token]);
    $response->assertStatus(403);

    $this->tokenData['aud'] = 'gauss';
    $this->stubData['findUserByEmail'] = (int) UserAuthorization::find(1)->identification;

    // Check that the token is expired
    $this->tokenData['aud'] = 'gauss';
    $this->tokenData['exp'] = now()->timestamp - 50;
    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/sso', ['token' => $this->token]);
    $response->assertStatus(500);
  }

  public function testSignupWithSSO()
  {
    $this->userInfo['id'] = 9999;
    $this->userInfo['email'] = 'noneexistingemailin@exam.net';

    $this->tokenData['aud'] = 'gauss';
    $this->tokenData['userType'] = 'signup';

    $this->stubData['findUserByEmail'] = NULL;

    $this->setTokenAndStubsWithCurrentUserInfo();

    $response = $this->json('POST', 'api/v1/auth/sso', ['token' => $this->token]);
    $response->assertStatus(200);
    $response->assertJson(
      [
        'type' => 'signup',
        'signupCode' => DUMMY_LONG_CODE,
        'email' => $this->userInfo['email'],
      ],
    );

    $verification = EmailVerificationCode::where('email', $this->userInfo['email'])->first();

    $this->assertDatabaseHas('email_verification_codes', [
      'email' => $this->userInfo['email'],
      'signupCode' => $verification['signupCode'],
    ]);
    $this->assertEquals(Hash::check(DUMMY_LONG_CODE, $verification['signupCode']), TRUE);
  }

  public function testSignupWithSSOgeneric()
  {
    $this->userInfo['id'] = 9999;
    $this->userInfo['email'] = 'noneexistingemailin@gmail.com';

    $this->tokenData['aud'] = 'gauss';
    $this->tokenData['userType'] = 'signup';

    $this->stubData['findUserByEmail'] = NULL;

    $this->setTokenAndStubsWithCurrentUserInfo(FALSE);

    $response = $this->json('POST', 'api/v1/auth/sso', ['token' => $this->token]);
    $response->assertStatus(422);
  }

  public function setTokenAndStubsWithCurrentUserInfo($acceptable = TRUE)
  {
    $privateKey = env('TEACHIQ_SSO_PRIVATE_KEY');
    $claims = [
      'data' => [
        'email' => [
          0 => $this->userInfo['email'],
        ],
        'usertype' => $this->tokenData['userType'],
        'firstName' => $this->userInfo['firstName'],
        'lastName' => $this->userInfo['lastName'],
      ],

      'iat' => now()->timestamp,
      'nbf' => now()->timestamp,
      'exp' => $this->tokenData['exp'],
      'aud' => $this->tokenData['aud'],
    ];

    // Encode fake token from Exam.net with claims used in ExamAuthenticate
    $this->token = JWT::encode($claims, $privateKey, 'EdDSA');

    $this->examNetServiceStub = $this->createStub(ExamNetService::class);
    $this->app->instance(ExamNetService::class, $this->examNetServiceStub);

    $this->examNetServiceStub
      ->method('findUserByEmail')
      ->willReturn($this->stubData['findUserByEmail'])
    ;
    $this->examNetServiceStub
      ->method('getUserInfo')
      ->willReturn($this->userInfo)
    ;
    $this->examNetServiceStub
      ->method('isDomainAcceptable')
      ->willReturn($acceptable)
    ;
  }
}
