<?php

namespace Tests\Feature\Api\V1\Signup;

use App\Models\EmailVerificationCode;
use App\Models\UserAuthorization;
use App\Services\CodeGeneratorService;
use App\Services\ExamNetService;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

const DUMMY_LONG_CODE = '2f70697c-cd24-4e2e-a0c2-7e3fb8100b98';

class DummyCodeGeneratorService extends CodeGeneratorService
{
  public static function generateLongCode()
  {
    return DUMMY_LONG_CODE;
  }
}

/**
 * @internal
 *
 * @coversNothing
 */
class PasswordVerificationControllerTest extends TestCase
{
  use WithoutMiddleware;
  private $dummyEmail = 'ronny.alex@exam.net';

  public function setUp(): void
  {
    parent::setUp();

    $this->examNetServiceStub = $this->createStub(ExamNetService::class);
    $this->app->instance(ExamNetService::class, $this->examNetServiceStub);

    $mock = new DummyCodeGeneratorService();
    $this->app->instance(CodeGeneratorService::class, $mock);

    $this->examNetServiceStub
      ->method('findUserByEmail')
      ->willReturn((int) UserAuthorization::find(1)->identification) // Real user. Needed to get the userId for auditEventService
    ;
    $this->examNetServiceStub
      ->method('setUserPassword')
    ;
  }

  public function testPasswordVerification()
  {
    // Request new password
    $response = $this->json('POST', '/api/v1/password/request', ['email' => $this->dummyEmail]);
    $response->assertStatus(204);
    $verification = EmailVerificationCode::where('email', $this->dummyEmail)->first();

    $this->assertEquals(Hash::check(DUMMY_LONG_CODE, $verification['longCode']), TRUE);
    $this->assertEquals($verification['email'], $this->dummyEmail);

    // Disapprove new password request
    $response = $this->json('POST', '/api/v1/password/verify', ['email' => $this->dummyEmail, 'code' => 'wrong-code']);
    $response->assertStatus(422);

    // Verify and approve new password request
    $response = $this->json('POST', '/api/v1/password/verify', ['email' => $this->dummyEmail, 'code' => DUMMY_LONG_CODE]);
    $response->assertStatus(204);

    // Fail Change password because of wrong code
    $response = $this->json('POST', '/api/v1/password/change', ['email' => $this->dummyEmail, 'code' => 'wrong-code', 'password' => 'I32k.dri']);
    $response->assertStatus(400);
    // Fail Change password because of wrong email
    $response = $this->json('POST', '/api/v1/password/change', ['email' => 'wrong@email.se', 'code' => 'wrong-code', 'password' => 'I32k.dri']);
    $response->assertStatus(401);

    // Change password successfully
    $response = $this->json('POST', '/api/v1/password/change', ['email' => $this->dummyEmail, 'code' => DUMMY_LONG_CODE, 'password' => 'I32k.dri']);
    $response->assertStatus(204);

    // Change password already done and EmailVerificationCode is deleted
    $response = $this->json('POST', '/api/v1/password/change', ['email' => $this->dummyEmail, 'code' => DUMMY_LONG_CODE, 'password' => 'I32k.dri']);
    $response->assertStatus(401);
  }
}
