<?php

namespace Tests\Feature\Api\V1\Auth;

use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class FeedbackControllerTest extends TestCase
{
  public function testFeedbackEmail()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('POST', '/api/v1/me/feedback', [
      'rating' => '5',
      'comment' => 'hej',
    ]);
    $response->assertStatus(204);
  }
}
