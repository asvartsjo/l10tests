<?php

namespace Tests\Feature\Api\V1\Me;

use App\Enums\UserVerificationStatus;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ProfileControllerTest extends TestCase
{
  /**
   * Missing authorization header - GET.
   */
  public function testProfileControllerGetRequiresAuthorizationHeader()
  {
    $response = $this->json('GET', '/api/v1/me/profile');
    $response->assertStatus(401);
  }

  /**
   * Admin can get its user profile.
   */
  public function testAdminCanGetItsUserProfile()
  {
    $headers = $this->adminAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('GET', '/api/v1/me/profile');

    $response->assertStatus(200);
    $response->assertJson([
      'firstName' => 'Admin',
      'lastName' => 'User',
      'countryId' => 'SE',
      'verified' => UserVerificationStatus::Teacher->value,
      'organizations' => [],
    ]);
  }

  /**
   * User can get its user profile.
   */
  public function testUserCanGetItsUserProfile()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('GET', '/api/v1/me/profile');

    $response->assertStatus(200);
    $response->assertJson([
      'firstName' => 'User',
      'lastName' => 'User',
      'countryId' => 'SE',
      'verified' => UserVerificationStatus::ToBeVerified->value,
      'organizations' => [],
    ]);
  }
}
