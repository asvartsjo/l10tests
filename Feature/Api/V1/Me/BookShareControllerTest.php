<?php

namespace Tests\Feature\Api\V1\Me;

use App\Enums\NodeAttributeType;
use App\Enums\NodeType;
use App\Models\Node;
use App\Models\NodeAttribute;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class BookShareControllerTest extends TestCase
{
  public function setUp(): void
  {
    parent::setUp();

    // Create curricula
    (new Node([
      'id' => 600233,
      'type' => NodeType::Book,
      'kmId' => 5824,
      'sort' => 377,
      'name' => 'Vectors',
    ]))->save();
    (new Node([
      'id' => 600234,
      'type' => NodeType::Book,
      'kmId' => 5824,
      'sort' => 377,
      'name' => 'Vectorsss',
    ]))->save();
    (new Node([
      'id' => 600235,
      'type' => NodeType::Book,
      'kmId' => 5824,
      'sort' => 377,
      'name' => 'Vectorsss',
    ]))->save();

    (new NodeAttribute([
      'nodeId' => 600233,
      'type' => NodeAttributeType::BookShareCode,
      'value' => 'N2R6XGV3',
    ]))->save();
    (new NodeAttribute([
      'nodeId' => 600234,
      'type' => NodeAttributeType::BookShareCode,
      'value' => 'M7G375ST',
    ]))->save();
    (new NodeAttribute([
      'nodeId' => 600235,
      'type' => NodeAttributeType::BookShareCode,
      'value' => 'K6666667',
    ]))->save();
    User::find(3)->books()->attach(600233, ['role' => 'OWNER']);
    User::find(3)->books()->attach(600234, ['role' => 'VIEWER']);
    // User::find(1)->books()->attach(600235, ['role' => 'OWNER']);
  }

  /**
   * Missing authorization header - GET.
   */
  public function testProfileControllerGetRequiresAuthorizationHeader()
  {
    $response = $this->json('GET', 'api/v1/book/node/40000/share');
    $response->assertStatus(401);
  }

  /**
   * Missing book - GET.
   */
  public function testBookShareControllerMissingBook()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('GET', 'api/v1/book/node/40000/share');

    $response->assertStatus(404);
  }

  /**
   * Missing Role - GET.
   */
  public function testBookShareControllerForbiddenRequest()
  {
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('GET', 'api/v1/book/node/600234/share');
    $response->assertStatus(403);
  }

  /**
   * User can get correct share code of book. - GET.
   */
  public function testUserCanGetShareCodeOfBook()
  {
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('GET', 'api/v1/book/node/600233/share');
    $response->assertStatus(200);

    $obj = $this->json('GET', 'api/v1/book/node/600233/share');
    $this->assertEquals('N2R6XGV3', $obj['code']);
  }

  /**
   * User can add shared book. - POST.
   */
  public function testUserCanAddSharedBook()
  {
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('POST', 'api/v1/book/node/600235/code/K6666667');
    $response->assertStatus(200);

    $this->assertDatabaseHas('node_user', [
      'userId' => 3,
      'nodeId' => 600235,
      'role' => 'VIEWER',
    ]);
  }

  /**
   * No book found - POST.
   */
  public function testCannotAddSharedBookMissingBook()
  {
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('POST', 'api/v1/book/node/7777777/code/Usd2d6667');

    $response->assertStatus(403);
  }

  /**
   * User already has book - POST.
   */
  public function testCannotAddSharedBookAlreadyHasBook()
  {
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('POST', 'api/v1/book/node/600233/code/N2R6XGV3');

    $response->assertStatus(200);
    $response->assertJson(
      fn (AssertableJson $json) => $json->where('successful', TRUE)->where('alreadyAdded', TRUE)
    );
  }
}
