<?php

namespace Tests\Feature\Api\V1\Me;

use Tests\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class CourseControllerTest extends TestCase
{
  /**
   * Missing authorization header - GET.
   */
  public function testProfileControllerGetRequiresAuthorizationHeader()
  {
    $response = $this->json('GET', 'api/v1/me/courses');
    $response->assertStatus(401);
  }

  /**
   * User can get courses with books - GET.
   */
  public function testUserCanGetCoursesWithBooks()
  {
    $this->markTestSkipped('Controller not sanely implemented yet');
    $headers = $this->userAuthorizationHeader();

    $response = $this->withHeaders($headers)->json('GET', 'api/v1/me/courses');
    $response->assertStatus(200);

    $obj = $this->json('GET', 'api/v1/me/courses');
    $this->assertEquals('Ma1a', $obj['courses'][0]['name']);
  }
}
