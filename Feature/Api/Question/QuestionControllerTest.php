<?php

namespace Tests\Feature\Api\V1\Question;

use App\Enums\NodeType;
use App\Models\Node;
use App\Models\Question;
use App\Services\CognitiveSearchService;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DummyCognitiveSearchService extends CognitiveSearchService
{
  public function sync(Question $question)
  {
  }
}

/**
 * @internal
 *
 * @coversNothing
 */
class QuestionControllerTest extends TestCase
{
  public function setUp(): void
  {
    parent::setUp();

    $mock = new DummyCognitiveSearchService();
    $this->app->instance(CognitiveSearchService::class, $mock);
  }

  public function testRequiresAuth()
  {
    $response = $this->json('GET', 'api/v1/question/1');
    $response->assertStatus(401);
  }

  public function testBasicGet()
  {
    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('GET', 'api/v1/question/1');
    $response->assertStatus(200);

    $response->assertJson(
      fn (AssertableJson $json) => $json->where('id', 1)
        ->whereType('version', 'integer')
        ->whereType('questionVersionId', 'integer')
        ->where('latest', TRUE)
        ->whereType('content', 'array')
        ->whereType('context', 'array')
        ->whereType('source', 'array')
    );
  }

  public function testBasicCreate()
  {
    $subchapter = Node::where('type', NodeType::Subchapter)->first();

    $question = $this->generateQuestionData();
    $question['subchapterId'] = $subchapter->id;

    $headers = $this->userAuthorizationHeader();
    $response = $this->withHeaders($headers)->json('POST', 'api/v1/question/', $question);
    $response->assertStatus(200);

    $response->assertJson(
      fn (AssertableJson $json) => $json->whereType('id', 'integer')
        ->where('version', 1)
        ->whereType('questionVersionId', 'integer')
        ->where('latest', TRUE)
        ->whereType('content', 'array')
        ->whereType('context', 'array')
        ->whereType('source', 'array')
    );
  }

  public function testBasicCreateAndUpdate()
  {
    $subchapter = Node::where('type', NodeType::Subchapter)->first();

    $questionData = $this->generateQuestionData();
    $question1 = $questionData;
    $question1['subchapterId'] = $subchapter->id;

    $headers = $this->userAuthorizationHeader();
    $response1 = $this->withHeaders($headers)->json('POST', 'api/v1/question/', $question1);
    $questionId = $response1['id'];

    $question2 = $questionData;
    $question2['content'][0] = '{}';

    $response3 = $this->withHeaders($headers)->json('PUT', 'api/v1/question/'.$questionId, $question2);
    $response3->assertStatus(200);

    $headers2 = $this->adminAuthorizationHeader();
    $response2 = $this->withHeaders($headers2)->json('PUT', 'api/v1/question/'.$questionId, $question2);
    $response2->assertStatus(200);
    $response2->assertJson(
      fn (AssertableJson $json) => $json
        ->whereType('id', 'integer')
        ->where('version', 3)
        ->whereType('questionVersionId', 'integer')
        ->where('latest', TRUE)
        ->whereType('content', 'array')
        ->whereType('context', 'array')
        ->whereType('source', 'array')
    );
  }

  protected function generateQuestionData()
  {
    return [
      'context' => [
        'public' => FALSE,
        'studentAccess' => TRUE,
        'calculator' => TRUE,
        'autocorrect' => TRUE,
        'type' => 'NORMAL',
      ],
      'content' => [['isOk' => TRUE]],
    ];
  }
}
