<?php

namespace Tests\Extension;

use PHPUnit\Event\TestRunner\Finished;
use PHPUnit\Event\TestRunner\FinishedSubscriber;
use PHPUnit\Event\TestRunner\Started;
use PHPUnit\Event\TestRunner\StartedSubscriber;
use PHPUnit\Runner\Extension\Facade as EventFacade;
use PHPUnit\Runner\Extension\ParameterCollection;
use PHPUnit\TextUI\Configuration\Configuration;

class Bootstrap
{
  public function executeBeforeFirstTest(): void
  {
    dd(1);
  }

  public function executeAfterLastTest(): void
  {
    dd(2);
  }

  public function bootstrap(Configuration $configuration, EventFacade $facade, ParameterCollection $parameters): void
  {
    $facade->registerSubscriber(new class($this) implements StartedSubscriber {
      public function __construct(private Bootstrap $thisClass)
      {
        dd(123);
      }

      public function notify(Started $event): void
      {
        dd(1);
        $this->thisClass->executeBeforeFirstTest();
      }
    });
    $facade->registerSubscriber(new class($this) implements FinishedSubscriber {
      public function __construct(private Bootstrap $thisClass)
      {
        dd(1);
      }

      public function notify(Finished $event): void
      {
        $this->thisClass->executeAfterLastTest();
      }
    });
  }
}
